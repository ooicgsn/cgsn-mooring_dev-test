## cgsn-mooring_dev-test

This is a temporary repository to hold testing configuration (Protobuf `*.pb.cfg` and YAML `*.yml` files) until the new YAML-based configuration repository is designed and completed.

Currently, barnaclese.whoi.edu is run from the files in `test/yaml/barnaclese`.

